use bevy::prelude::*;

fn main() {
    info!("Starting launcher: Native");
    test_bevy::app(true).run();
}
